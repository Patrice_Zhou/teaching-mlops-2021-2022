# MLOPS : Nabet & Zhou

## Sujet 

https://tinyurl.com/mlops-paper

## Lien du site 
L'url du site est le suivant : 
https://teaching-mlops-dayone.herokuapp.com/ \
Le code principal se trouve dans le fichier ***src/main_controller.py***

## Tests 

Les tests unitaires se trouvent dans le fichier **src/test_controller.py** \
Les tests d'intégrations se trouvent dans le fichier ***test_e2e*** 

## Benchmark 
### Taille de l'environnement
![ram](images/ram.png) \
![size](images/env_size.png) \
Les images précédentes nous donnent les informations sur la taille de l'image et de la ram utilisée. L'image fait 439 MB et utilise 284.3 MB de RAM. Il faut alors une ram de 512 MB pour nos besoins.

Pour obtenir l'info du P99, nous avons effectué plusieurs stresstests. Nous avons gardé le test effectué avec locust et le fichier ***stress_test/locustfile_model_only.py***.

### Stresstests

Parmi les stresstests, j'avais considéré des agents qui pourraient accéder à toutes les pages avec des temps d'attentes avant la prochaine action pour simuler un comportement humain. Néanmoins, j'ai remarqué que ces résultats étaient en réalité très variable et ne montrait pas forcément le trafic pour notre modèle de ML. J'ai donc décidé de me concentrer sur le chemin **/api/intent?sentence=''** et sans aucun temps d'attente entre chaque action d'agent. Toutefois, lorsque je faisais cela, les temps de réponses commençaient à diminuer malgré des requêtes qui augmentent. Je suspecte alors que la page a été sauvegardé en mémoire pour le renvoyer au lieu de refaire appel à notre modèle. Dans le doute, j'ai effectué un stress test en changeant le paramètre *sentence* pour m'assurer que le modèle soit appelé. \
![requests](images/total_requests.png) \
![response](images/response_times.png) \
D'après les graphes données par Locust plus haut. Nous pouvons remarquer qu'on a un P95 < 170 ms (la ligne jaune) pour environ 230 requêtes/s. On peut considérer alors un P99 < 200ms pour un trafic aux alentours de 230 requêtes/s

## Pipeline
Dans l'énoncé, il est indiqué de faire une pipeline sous la forme Test -> Build -> Deploy. Dans notre cas, nous avons proposé une solution où nous construisons les images docker dans l'étape 1. En effet, afin de réaliser les tests unitaires, il faut installer l'ensemble des paquets dans l'environnement d'exécution de la pipeline. Afin de réaliser cette étape une seule fois, nous avons décidé de réaliser le build de l'image docker en premier. Ainsi, l'ensemble des paquets ne sont installés qu'une seule fois, puis nous pouvons récupérer cette image à chaque étape afin de pouvoir exécuter n'importe quelle commande dans le même environnement que l'environnement de développement. Lors de build, nous créons 2 images en parallèles. Une image de test qui comprend l'ensemble des packages ainsi qu'une image de release qui comprend uniquement les packages nécessaires au déploiement. Cela permet de gagner du temps et d'éviter d'installer des packages plusieurs fois.


## Améliorations

On essaye de régler un problème de large volume de données à traiter en offline. Dans notre implémentation actuelle, nous avons un unique modèle avec un seul worker qui traite les requêtes de manière séquentielle une par une. Afin de gagner en efficacité, nous pourrions traiter les requêtes plutôt par lot. Notons **BATCH_SIZE** le nombre d'input que le modèle peut traiter d'un coup. Nous pourrions utiliser un système de cache où lorsque qu'un input arrive, si le modèle est occupé à calculer, les informations sont stockées dans le cache. Lorsque le modèle fini un calcul, il regarde les inputs en attente dans le cache et en dépile au plus **BATCH_SIZE** pour les traiter d'un coup. Cela permet de traiter les requêtes **BATCH_SIZE** par **BATCH_SIZE** au lieu de une par une, mais cela demande d'utiliser plus de RAM pour stocker les inputs temporaires et en traiter plus d'un coup.
De plus, nous pourrions aussi utiliser plusieurs workers au lieu d'un seul afin de distribuer la demande de calcul sur plusieurs machines.
