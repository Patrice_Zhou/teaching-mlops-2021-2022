from fastapi import FastAPI
import spacy
import logging
from fastapi.responses import JSONResponse

app = FastAPI()
logging.info("Loading model..")
nlp = spacy.load("./models")


@app.get("/api/intent")
def intent_inference(sentence: str):
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return JSONResponse(content=inference.cats)


@app.get("/api/intent-supported-languages")
def supported_languages():
    return JSONResponse(content=["fr-FR"])


@app.get("/health")
def health_check_endpoint():
    return JSONResponse(status_code=200)
