from src.main_controller import supported_languages, intent_inference
import pytest


@pytest.mark.parametrize("sentence, expected", [
    ('Helloworld', 200),
    ('sentence', 200),
    ('', 200)
])
def test_intent_with_sentence(sentence, expected):
    response = intent_inference(sentence)
    assert response.status_code == expected


@pytest.mark.parametrize("sentence, expected", [
    ('Hello world', "application/json"),
    ('sentence', "application/json"),
    ('', "application/json")
])
def test_intent_response_type(sentence, expected):
    response = intent_inference(sentence)
    assert response.headers["Content-Type"] == expected


def test_supported_languages_response_type():
    response = supported_languages()
    assert response.headers["Content-Type"] == "application/json"
