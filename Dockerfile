FROM python:3.7-slim-buster

ENV YOUR_ENV= PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  PYTHONPATH=/app


WORKDIR /app

RUN pip install 'poetry==1.1.12'
COPY poetry.lock pyproject.toml /app/
#No venv needed as docker images are still isolated
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev


COPY . /app/


#test files are only needed for docker test file
RUN rm -fr test_e2e/ stress_test/
RUN rm mypy.ini


ENV PORT=8080
CMD ["bash", "start.sh"]
#Expose command in not handled in heroku. We need to set the PORT env variable
#Heroku will then map this port of the image to the port 80 of the web server
