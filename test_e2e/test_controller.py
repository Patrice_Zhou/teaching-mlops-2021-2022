import requests


# intent
def test_intent_without_sentence():
    response = requests.get("http://127.0.0.1:8080/api/intent")
    assert response.status_code == 422


def test_intent_with_sentence():
    response = requests.get(
        f"http://127.0.0.1:8080/api/intent?sentence=HelloWorld"
    )
    assert response.status_code == 200


def test_supported_languages_key():
    response = requests.get("http://127.0.0.1:8080/api/intent?sentence=")
    keys = [
        'purchase', 'find-restaurant', 'irrelevant', 'find-hotel',
        'provide-showtimes', 'find-around-me', 'find-train', 'find-flight'
    ]
    assert keys.sort() == list(response.json().keys()).sort()


# supported languages
def test_supported_languages_response_type():
    response = requests.get(
        "http://127.0.0.1:8080/api/intent-supported-languages"
    )
    assert response.headers["Content-Type"] == "application/json"


def test_supported_languages_response_body():
    response = requests.get(
        "http://127.0.0.1:8080/api/intent-supported-languages"
    )
    assert response.json() == ['fr-FR']
