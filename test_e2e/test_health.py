import requests


def test_health_check():
    response = requests.get("http://0.0.0.0:8080/health")
    assert response.status_code == 200
