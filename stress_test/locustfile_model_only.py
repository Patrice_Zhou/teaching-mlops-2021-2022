from locust import HttpUser, between, task
import random
import string


class WebsiteUser(HttpUser):

    def on_start(self):
        self.client.get(
            "api/intent?sentence="
        )

    def generate_string(self):
        length = random.randrange(1, 10)
        res_str = ''.join(
            random.choice(string.ascii_lowercase)
            for _ in range(length)
        )
        return res_str

    @task
    def intent(self):
        sentence = self.generate_string()
        self.client.get("api/intent?sentence=" + sentence)
