from locust import HttpUser, between, task


class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    def on_start(self):
        self.client.get(
            "health"
        )

    @task
    def intent(self):
        self.client.get("api/intent?sentence=")

    @task
    def supported_lang(self):
        self.client.get("api/intent-supported-languages")
